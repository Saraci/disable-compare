<?php
/**
 * Copyright (c) 2017 Sherodesigns.
 * *
 * @date 5/10/2017
 * @author Sherodesigns | Besim Saraci
 * @link https://www.sherodesigns.com/
 * @module Compare_Helper
 * @class Compare_Helper_Data
 */

namespace Shero\Compare\Helper;


use Magento\Framework\App\Helper\Context;

/**
 * Class Data
 * @package Shero\Compare\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    const CATEGORY_COMPARE_CSS = 'compare/general/css_category' ;
    const PRODUCT_COMPARE_CSS = 'compare/general/css_product';

    public function __construct(
        Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->_scopeConfig = $scopeConfig;
        parent::__construct($context);
    }

    /**
     * @return mixed
     */
    public function getCompareCssProductPage()
    {
        $productCss =  $this->_scopeConfig->getValue(self::PRODUCT_COMPARE_CSS, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        return $productCss;
    }

    /**
     * @return mixed
     */
    public function getCompareCssCategoryPage()
    {
        $categoryCss =  $this->_scopeConfig->getValue(self::CATEGORY_COMPARE_CSS, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        return $categoryCss;
    }
}
