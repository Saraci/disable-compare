# README #

# Disable Compare #

## *Version 1.0.0 - June 5, 2017* ##

This extension is used to disable compare functionality from a Magento store. It removes all parts where this functionality is shown:

* Compare Products block in sidebar
* Compare link in category view
* Compare link in product page

![Shero_Template_-_Google_Docs.png](https://bitbucket.org/repo/Bgg6GaR/images/1644098621-Shero_Template_-_Google_Docs.png)

This is how the menu in admin looks. The link to extension configurations is put together with other Shero Extensions, using a base module for that.

## How to use: ##

1. The removal of the sidebar block is done automatically, no need to do any configuration to achieve that.
1. For compare links the user has to find the css classes for all the places where they are shown in the frontend and put them in the admin config fields. The admin configuration looks like this:

![1.png](https://bitbucket.org/repo/Bgg6GaR/images/1223879192-1.png)

**Css class in product page:** inspect the compare link in product page and find the class property and put it on the respective field (don’t forget . before the class name).

**Css class in category page**: inspect the compare link in product page and find the class property and put it on the respective field (don’t forget . before the class name).

## Result: ##

![1.png](https://bitbucket.org/repo/Bgg6GaR/images/2894829009-1.png)

**As you can see the compare functionality is fully removed from frontend, but you also can remove it partially (only the main block), by leaving css fields empty in admin or also disable the module if you do not want it to work anymore.**

*Developer:  Besim Saraci*