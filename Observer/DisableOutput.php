<?php
/**
 * Copyright (c) 2017 Sherodesigns.
 * *
 * @date 5/10/2017
 * @author Sherodesigns | Besim Saraci
 * @link https://www.sherodesigns.com/
 * @module Compare_Observer
 * @class Compare_Observer_DisableOutput
 */

namespace Shero\Compare\Observer;

use Magento\Config\Model\ResourceModel\Config;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;

/**
 * Class DisableOutput
 * @package Shero\Compare\Observer
 */
class DisableOutput implements \Magento\Framework\Event\ObserverInterface
{

    const SHERO_CONFIG = 'compare/general/enable';
    /**
     * @var \Magento\Config\Model\ResourceModel\Config
     */
    protected $_config;
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;


    public function  __construct(
        Config $_config,
        \Magento\Framework\App\Config\ScopeConfigInterface $_scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\RequestInterface $request
    ){
        $this->_config = $_config;
        $this->_scopeConfig = $_scopeConfig;
        $this->storeManager = $storeManager;
        $this->request = $request;

    }

    /**
     * @param Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $disable = false;
        $scopeType = \Magento\Framework\App\Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT;
        $scopeCode = 0;


        /**
         * find the config value for the current admin scope
         */
        if($this->request->getParam(\Magento\Store\Model\ScopeInterface::SCOPE_STORE))
        {
            $scopeType = ScopeInterface::SCOPE_STORE;
            $scopeCode = $this->storeManager->getStore($this->request->getParam(\Magento\Store\Model\ScopeInterface::SCOPE_STORE))->getCode();
        }elseif($this->request->getParam(\Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE))
        {
            $scopeType = ScopeInterface::SCOPE_WEBSITE;
            $scopeCode = $this->storeManager->getWebsite($this->request->getParam(\Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE))->getCode();
        }
        else
        {
            $scopeType = \Magento\Framework\App\Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT;
            $scopeCode = 0;
        }
        $moduleConfig= $this->_scopeConfig->getValue(self::SHERO_CONFIG, $scopeType);


        /**
         * if module disabled from config, disable output config
         */

        if(!$moduleConfig){
            $disable = true;
        }

        $moduleName = 'Shero_Compare';
        $outputPath = "advanced/modules_disable_output/$moduleName";

        $this->_config->saveConfig($outputPath,$disable, $scopeType,$scopeCode);
    }
}

